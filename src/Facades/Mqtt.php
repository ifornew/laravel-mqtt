<?php

declare(strict_types=1);

namespace Ifornew\Mqtt\Facades;

use Illuminate\Support\Facades\Facade;
use Ifornew\Mqtt\ConnectionManager;
use PhpMqtt\Client\Contracts\MqttClient as MqttClientContract;
use PhpMqtt\Client\MqttClient;

/**
 * @method static MqttClientContract connection(string $name = null)
 * @method static void disconnect(string $connection = null)
 * @method static void publish(string $topic, string $message, int $qos = MqttClient::QOS_AT_MOST_ONCE, bool $retain = false, string $connection = null)
 *
 * @package PhpMqtt\Client\Facades
 * @see ConnectionManager
 */
class Mqtt extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ConnectionManager::class;
    }
}
